import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:latlong/latlong.dart';
import 'package:flutter_background_geolocation/flutter_background_geolocation.dart'
    as bg;

////
// For pretty-printing location JSON.  Not a requirement of flutter_background_geolocation
//
import 'dart:convert';
import 'dart:async';

JsonEncoder encoder = new JsonEncoder.withIndent("     ");
//
////
const ZOOM_LEVEL = 17.0;

typedef void OnActivityChanged(bool isEnabled);

class Map extends StatefulWidget {
  Map({Key key, this.initialPaths, this.trackingListenable}) : super(key: key);

  final List<LayerOptions> initialPaths;
  final Listenable trackingListenable;

  @override
  _MapState createState() => _MapState();
}

class _MapState extends State<Map> with TickerProviderStateMixin {
  static LatLng london = LatLng(51.5, -0.09);
  static LatLng paris = LatLng(48.8566, 2.3522);
  static LatLng dublin = LatLng(53.3498, -6.2603);
  bool _isMoving = false;

  bool _enabled = false;
  bool _isReady;
  String _motionActivity = 'UNKNOWN';
  String _odometer = '0';

  String _content = '';
  final MapController mapController = MapController();
  List<LatLng> currentPosition = <LatLng>[paris];
  List<LatLng> pathPoints = <LatLng>[];
  final StreamController<bool> _trackingNotifier =
      StreamController<bool>(sync: false);

  List<CircleMarker> get circleMarkers => currentPosition.map((latlng) {
        return CircleMarker(
            point: latlng,
            color: Colors.blue.withOpacity(0.9),
            borderColor: Colors.red.withOpacity(0.9),
            borderStrokeWidth: 1.0,
            radius: 7);
      }).toList();

  List<LatLng> get points => pathPoints.map((latlng) {
        return latlng;
      }).toList();

  @override
  void initState() {
    print('Init state');
    super.initState();

    // 1.  Listen to events (See docs for all 12 available events).
    bg.BackgroundGeolocation.onLocation(_onLocation);
    bg.BackgroundGeolocation.onActivityChange(_onActivityChange);
    bg.BackgroundGeolocation.onProviderChange(_onProviderChange);

    // 2.  Configure the plugin
    bg.BackgroundGeolocation.ready(bg.Config(
            desiredAccuracy: bg.Config.DESIRED_ACCURACY_HIGH,
            distanceFilter: 1,
            stopOnTerminate: true,
            startOnBoot: false,
            debug: false,
            logLevel: bg.Config.LOG_LEVEL_OFF,
            reset: true))
        .then((bg.State state) {
      print('state10500  $state');

      setState(() {
        _enabled = state.enabled;
        _isMoving = state.isMoving;
      });
    });

//    if (widget.isTrackingEnabled) {
//      bg.BackgroundGeolocation.start().then((bg.State state) {
//        bg.BackgroundGeolocation.changePace(true);
//        setState(() {
//          _enabled = state.enabled;
//          _isMoving = state.isMoving;
//        });
//        if (widget.onTrackingChanged != null) {
//          widget.onTrackingChanged(_enabled);
//        }
//        if (widget.onMovingChanged != null) {
//          widget.onMovingChanged(_isMoving);
//        }
//      });
////      _onClickChangePace();
//    } else {
//      bg.BackgroundGeolocation.stop().then((bg.State state) {
//        // Reset odometer.
//        bg.BackgroundGeolocation.setOdometer(0.0);
//        bg.BackgroundGeolocation.changePace(false);
//        setState(() {
//          _odometer = '0.0';
//          _enabled = state.enabled;
//          _isMoving = state.isMoving;
//        });
//        if (widget.onTrackingChanged != null) {
//          widget.onTrackingChanged(_enabled);
//        }
//        if (widget.onMovingChanged != null) {
//          widget.onMovingChanged(_isMoving);
//        }
//      });
//      _onClickChangePace();
//    }
  }

  // Manually toggle the tracking state:  moving vs stationary
  void _onClickChangePace() {
    setState(() {
      _isMoving = !_isMoving;
    });
    print('_isMoving $_isMoving');
    if (!_isMoving) {
      pathPoints.clear();
    }
    if (pathPoints.isNotEmpty) {
      pathPoints.clear();
    }
//    bg.BackgroundGeolocation.changePace(_isMoving).then((bool isMoving) {
//      print('[changePace] success $isMoving');
//    }).catchError((e) {
//      print('[changePace] ERROR: ' + e.code.toString());
//    });
  }

  // Manually fetch the current position.
  void _onClickGetCurrentPosition() {
    if (_isMoving) {
      return;
    }

    bg.BackgroundGeolocation.getCurrentPosition(
            persist: false, // <-- do not persist this location
            desiredAccuracy: 0, // <-- desire best possible accuracy
            timeout: 30000, // <-- wait 30s before giving up.
            samples: 1 // <-- sample 3 location before selecting best.
            )
        .then((bg.Location location) {
      print('[getCurrentPosition] - $location');
      // Just move to point
//      mapController.move(london, 20);
      // Animated move to point
//      _animatedMapMove(london, 16.0);
      final position =
          LatLng(location.coords.latitude, location.coords.longitude);
//      _animatedMapMove(position, ZOOM_LEVEL);
      _animatedMapMove(position, mapController.zoom);
      currentPosition.clear();
      pathPoints.clear();
      setState(() {
        currentPosition.add(position);
      });
    }).catchError((error) {
      print('[getCurrentPosition] ERROR: $error');
    });
  }

  void _onLocation(bg.Location location) {
    print('[location] - $location');

    final position =
        LatLng(location.coords.latitude, location.coords.longitude);
    currentPosition.clear();
//    _animatedMapMove(position, ZOOM_LEVEL);
//    mapController.move(position, mapController.zoom);
    _animatedMapMove(position, mapController.zoom);

    setState(() {
      currentPosition.add(position);
    });

    if (_isMoving) {
      setState(() {
        pathPoints.add(position);
      });
    }
  }

  void _onActivityChange(bg.ActivityChangeEvent event) {
    print('[activitychange] - $event');
    setState(() {
      _motionActivity = event.activity;
    });
  }

  void _onProviderChange(bg.ProviderChangeEvent event) {
    print('$event');

    setState(() {
      _content = encoder.convert(event.toMap());
    });
  }

  // Move to point
  void _animatedMapMove(LatLng destLocation, double destZoom) {
    // Create some tweens. These serve to split up the transition from one location to another.
    // In our case, we want to split the transition be<tween> our current map center and the destination.
    final _latTween = Tween<double>(
        begin: mapController.center.latitude, end: destLocation.latitude);
    final _lngTween = Tween<double>(
        begin: mapController.center.longitude, end: destLocation.longitude);
    final _zoomTween = Tween<double>(begin: mapController.zoom, end: destZoom);

    // Create a animation controller that has a duration and a TickerProvider.
    var controller = AnimationController(
        duration: const Duration(milliseconds: 500), vsync: this);
    // The animation determines what path the animation will take. You can try different Curves values, although I found
    // fastOutSlowIn to be my favorite.
    Animation<double> animation =
        CurvedAnimation(parent: controller, curve: Curves.fastOutSlowIn);

    controller.addListener(() {
      mapController.move(
          LatLng(_latTween.evaluate(animation), _lngTween.evaluate(animation)),
          _zoomTween.evaluate(animation));
    });

    animation.addStatusListener((status) {
      if (status == AnimationStatus.completed) {
        controller.dispose();
      } else if (status == AnimationStatus.dismissed) {
        controller.dispose();
      }
    });

    controller.forward();
  }

  @override
  Widget build(BuildContext context) {
    return FlutterMap(
        options: MapOptions(
//            center: currentPosition.first,
          zoom: ZOOM_LEVEL,
          minZoom: 3,
        ),
        mapController: mapController,
        layers: []
          ..addAll(widget.initialPaths)
          ..add(CircleLayerOptions(circles: circleMarkers))
          ..add(
            PolylineLayerOptions(
              polylines: [
                Polyline(
                    points: points, strokeWidth: 4.0, color: Colors.purple),
              ],
            ),
          ));
  }
}
