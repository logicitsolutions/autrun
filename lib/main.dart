import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';

import 'map.dart';
import 'mockData.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Vincent',
      theme: ThemeData(
        primarySwatch: Colors.orange,
      ),
      home: MyHomePage(title: 'Vincent'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  final List<LayerOptions> layersOptions = <LayerOptions>[
    TileLayerOptions(
      urlTemplate: "https://api.tiles.mapbox.com/v4/"
          "{id}/{z}/{x}/{y}@2x.png?access_token={accessToken}",
      additionalOptions: {
        'accessToken':
            'pk.eyJ1IjoiYWt5bmEiLCJhIjoiY2pqeHd1emkwMTRjeDNwbWt0NWpobXF6OCJ9.t96XJz180To76-seQUepug',
        'id': 'mapbox.dark',
      },
    ),
    PolylineLayerOptions(
      polylines: [
        Polyline(points: ways, strokeWidth: 3.0, color: Colors.red),
      ],
    ),
    PolylineLayerOptions(
      polylines: [
        Polyline(points: ways1, strokeWidth: 3.0, color: Colors.green),
      ],
    ),
    PolylineLayerOptions(
      polylines: [
        Polyline(points: ways3, strokeWidth: 3.0, color: Colors.orange),
      ],
    ),
    PolylineLayerOptions(
      polylines: [
        Polyline(points: way4, strokeWidth: 4.0, color: Colors.blue),
      ],
    )
  ];

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  bool _isTracking = false;
  bool _isMoving = false;

  @override
  Widget build(BuildContext context) {
    final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(title: Text(widget.title), actions: <Widget>[
        Switch(
            value: _isTracking,
            onChanged: (val) {
              setState(() {
                _isTracking = val;
              });
            }),
      ]),
      body: Map(
        initialPaths: widget.layersOptions,
      ),
//        bottomNavigationBar: BottomAppBar(
//            child: Container(
//                padding: const EdgeInsets.only(left: 5.0, right: 5.0),
//                child: Row(
//                    mainAxisSize: MainAxisSize.max,
//                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                    children: <Widget>[
//                      IconButton(
//                        icon: Icon(Icons.gps_fixed),
//                        onPressed:
//                        _isMoving ? null : _onClickGetCurrentPosition,
//                        disabledColor: Colors.grey[350],
//                      ),
////                      Text('$_motionActivity · $_odometer km'),
//                      MaterialButton(
//                          minWidth: 50.0,
//                          child: Icon(
//                              (_isMoving) ? Icons.pause : Icons.play_arrow,
//                              color: Colors.white),
//                          color: (_isMoving) ? Colors.red : Colors.green,
//                          onPressed: _onClickChangePace)
//                    ])))
    );
  }
}
